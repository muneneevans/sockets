﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
//using Newtonsoft.Json;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Collections.ObjectModel;
using sockets;

namespace CRMS
{
   


    public class Server
    {
        //private static DBManager DB1 = new DBManager();
        private static byte[] _buffer = new byte[10000];
        private static List<Socket> _clientsockets = new List<Socket>();
        private static Socket _serversocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);        
        
        

        public Server()
        {
            SetupServer();                        
        }

        #region server connection managers
        private static void SetupServer()
        {
            try
            {
                Debug.WriteLine("Setting up server");
                _serversocket.Bind(new IPEndPoint(IPAddress.Any,224));
                _serversocket.Listen(5);
                _serversocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
                Debug.WriteLine("Server set up");
            }
            catch
            {
                Debug.WriteLine("unable to Set up server");

            }
        }

        private static void AcceptCallback(IAsyncResult AR)
        {

            Socket socket = _serversocket.EndAccept(AR);
            _clientsockets.Add(socket);
            Debug.WriteLine("Client connected");
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            _serversocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

        }
        private static void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);

        }

        private static void SendMessage()
        {
        }

        private static void ReceiveCallback(IAsyncResult AR)
        {
            try
            {
                Socket socket = (Socket)AR.AsyncState;
                int received = socket.EndReceive(AR);
                byte[] databuf = new byte[received];
                Array.Copy(_buffer, databuf, received);

                string frame = Encoding.ASCII.GetString(databuf);
                Debug.WriteLine("Text received: " + frame);
                Message m = JsonConvert.DeserializeObject<Message>(frame);
                ((App)Application.Current).vm.AddMessage(m);
                string r = "no response";
                

                byte[] response = Encoding.UTF8.GetBytes(r);
                socket.BeginSend(response, 0, response.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
                //MessageBox.Show("sent response"); 
                //Thread.Sleep(5000);
                socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            }
            catch { }
        }
        #endregion
        

    }
}