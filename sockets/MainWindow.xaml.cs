﻿using CRMS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace sockets
{

    public class Message : INotifyPropertyChanged
    {
        private string _sender;
        public string Sender
        {
            get
            {
                return _sender;
            }
            set
            {
                _sender = value;
                NotifyPropertyChanged("Sender");
            }
        }
        private string _content;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                NotifyPropertyChanged("Content");
            }
        }


    }
    public class ViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Message> AllMessages { get; set; }

        public ViewModel()
        {
            AllMessages = new ObservableCollection<Message>();
        }

        public void AddMessage(Message NewMessage)
        {
            try
            {
                Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.DataBind,
                (SendOrPostCallback)delegate { AllMessages.Add(NewMessage); },
                null);
                Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.DataBind,
               (SendOrPostCallback)delegate { NotifyPropertyChanged("AllMessages"); },
               null);

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Debug.WriteLine(e.InnerException);
            }
        }

        //variable: event raised when a class property changes
        public event PropertyChangedEventHandler PropertyChanged;
        //function: default property for to notify property
        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private static DBManager DB1 = new DBManager();
        private static byte[] _buffer = new byte[10000];
        private static List<Socket> _clientsockets = new List<Socket>();
        private static Socket _serversocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public MainWindow()
        {
            InitializeComponent();
            DataContext = ((App)Application.Current).vm;

            IPAddress IP = IPAddress.Parse("127.0.0.1");

            // Local address to listen.

            TcpListener Listener = new TcpListener(IP, 224);
            // Listening from TCP clients.
            Listener.Start(); // Starts listening for incoming requests.
            Console.WriteLine("Server is up at port 1986");
            Console.WriteLine("Waiting for the connection...");
            ClientClass user = new ClientClass(Listener.AcceptTcpClient());

            string Message = Console.ReadLine();
            user.SendMessage(Message);
        }


        class ClientClass
        {
            TcpClient Client;
            byte[] Data;
            bool Flag = true;
            NetworkStream netStream;

            // When a client comes in.
            public ClientClass(TcpClient newClient)
            {

                Client = newClient;

                Console.WriteLine("Connected with " + Client.Client.RemoteEndPoint);

                // To notify the client about his connection.
                SendMessage("Connected.");

                // For receiving the data.       
                Data = new byte[100];
                netStream = Client.GetStream();
                netStream.BeginRead(Data, 0, 100, ReceiveMessage, null);
            }

            // Read from clients.
            public void ReceiveMessage(IAsyncResult ar)
            {
                int bufferLength;
                try
                {
                    bufferLength = Client.GetStream().EndRead(ar);

                    // Receive the message from client side.
                    string messageReceived = Encoding.ASCII.GetString(Data, 0, bufferLength);
                    Message m = JsonConvert.DeserializeObject<Message>(messageReceived);
                    //MessageBox.Show("Sender: " + m.Sender + "\nMessage: " + m.Content);

                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate 
                    {
                        ((App)Application.Current).vm.AddMessage(m);
                    }));
                    // Display it on server's console.
                    Console.WriteLine(Client.Client.RemoteEndPoint.ToString() + "-> " + messageReceived);


                    // Continue reading from client. 
                    Client.GetStream().BeginRead(Data, 0, 100, ReceiveMessage, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(Client.Client.RemoteEndPoint.ToString() + " is disconnected.");
                }
            }

            // Send the message.
            public void SendMessage(string message)
            {
                try
                {
                    byte[] bytesToSend = System.Text.Encoding.ASCII.GetBytes(message);
                    Client.Client.Send(bytesToSend);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }


    }
}
