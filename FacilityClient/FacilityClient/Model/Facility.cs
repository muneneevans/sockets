﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacilityClient.Model
{
    public class Facility : INotifyPropertyChanged
    {

        private int _facility_id; 
        public int facility_id
        {
            get
            {
                return _facility_id;
            }
            set
            {
                _facility_id = value;
                NotifyPropertyChanged("facility_id");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
