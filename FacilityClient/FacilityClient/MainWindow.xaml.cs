﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FacilityClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        SQLiteConnection conn = new SQLiteConnection("Data Source=Student.db");

        public MainWindow()
        {
            InitializeComponent();
            Debug.WriteLine("connecting to db");
            SQLite();
        }

        private void SQLite()
        {
            try
            {
                //connect to sqlite
                conn.Open();
                var command = conn.CreateCommand();
                //table Create
                command.CommandText = "CREATE TABLE IF NOT EXISTS facility(id int, Name TEXT)";
                command.ExecuteNonQuery();

                //Inserting data
                command.CommandText = "INSERT INTO facility(id, Name ) values (1, 'Nairobi')";
                command.ExecuteNonQuery();

                //Inserting data
                command.CommandText = "INSERT INTO facility(id, Name ) values (2, 'Nairobi')";
                command.ExecuteNonQuery();
                //Inserting data
                command.CommandText = "INSERT INTO facility(id, Name ) values (3, 'Nairobi')";
                command.ExecuteNonQuery();
                //Inserting data
                command.CommandText = "INSERT INTO facility(id, Name ) values (4, 'Nairobi')";
                command.ExecuteNonQuery();


                //Read from table
                command.CommandText = "SELECT * FROM facility";
                SQLiteDataReader sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    Debug.WriteLine(sdr.GetString(1));
                }
                sdr.Close();





                //table Update
                command.CommandText = "UPDATE facility SET Name='Nakuru' WHERE id = 1";
                command.ExecuteNonQuery();

                //Read from table
                command.CommandText = "SELECT * FROM facility";
                sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    Debug.WriteLine(sdr.GetString(1));
                }
                sdr.Close();

                //Delete Record
                command.CommandText = "DELETE FROM std_tbl WHERE id=1";
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {


                throw;
            }
        }
    }
}
