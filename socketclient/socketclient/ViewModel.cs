﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace socketclient
{
    public class Message : INotifyPropertyChanged
    {
        private string _sender;
        public string Sender
        {
            get
            {
                return _sender;
            }
            set
            {
                _sender = value;
                NotifyPropertyChanged("Sender");
            }
        }
        private string _content;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                NotifyPropertyChanged("Content");
            }
        }

        
    }
    public class ViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Message> AllMessages { get; set; }

        public ViewModel()
        {
            AllMessages = new ObservableCollection<Message>();
        }

        public void AddMessage(Message NewMessage)
        {
            AllMessages.Add(NewMessage);
            NotifyPropertyChanged("AllMessages");            
        }

        //variable: event raised when a class property changes
        public event PropertyChangedEventHandler PropertyChanged;
        //function: default property for to notify property
        protected void NotifyPropertyChanged(string PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }
}
