﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace socketclient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 



    public partial class MainWindow : Window
    {
        //private SocketPermission socksender = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp , "",SocketPermission.AllPorts);

        TcpClient Client;
        byte[] Data;
        ObservableCollection<Message> AllMessages = new ObservableCollection<Message>();
        private bool IsConnected = false;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = ((App)Application.Current).vm;

            
        }


        public void Setup()
        {
            Client = new TcpClient();
            Client.Connect(ServerIpTextBox.Text, 224);
            Data = new byte[100];
            IsConnected = true;
            //MessageBox.Show("Message has been sent");
            // Reading from server.
            Client.GetStream().BeginRead(Data, 0, 100, ReceiveMessage, null);
        }
        public void SendMessage(string message)
        {
            try
            {

                NetworkStream netStr = Client.GetStream();
                byte[] Data = System.Text.Encoding.ASCII.GetBytes(message);
                netStr.Write(Data, 0, Data.Length);
                //MessageBox.Show("Message has been sent");
                netStr.Flush();

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

        }

        public void ReceiveMessage(IAsyncResult ar)
        {
            int bufferLength;
            try
            {
                bufferLength = Client.GetStream().EndRead(ar);

                string message = (System.Text.Encoding.ASCII.GetString(Data, 0, bufferLength)).ToString();

                Debug.WriteLine(Client.Client.RemoteEndPoint.ToString()+ "-> " + message);

                // Continue reading from the server.
                Client.GetStream().BeginRead(Data, 0, 100, ReceiveMessage, null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsConnected)
            {
                Setup();
            }

            Message m = new Message();
            m.Sender = SenderTextBox.Text;
            m.Content = MessageTextBox.Text;

            ((App)Application.Current).vm.AddMessage(m);
            SendMessage(JsonConvert.SerializeObject(m));
            //MessagesListView.ItemsSource = ((App)Application.Current).vm.AllMessages;
        }
    }
}
